# Pulls changes from forks
# Run before any works
Start-Process -FilePath "git" -ArgumentList "remote add github https://github.com/anujdutt9/Handwritten-Digit-Recognition-using-Deep-Learning.git" -NoNewWindow -Wait
Start-Process -FilePath "git" -ArgumentList "pull github master" -NoNewWindow -Wait
Start-Process -FilePath "git" -ArgumentList "remote add github-fork https://github.com/Baneeishaque/Handwritten-Digit-Recognition-using-Deep-Learning.git" -NoNewWindow -Wait
Start-Process -FilePath "git" -ArgumentList "pull github-fork master" -NoNewWindow -Wait
